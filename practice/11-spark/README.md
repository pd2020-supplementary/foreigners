# How to run the notebook

Use run_notebook.sh script, fill the empty fields:
 - PORT - port, where the notebook will open
 - TOKEN - token for entering the Jupyter (any string)
 - N - number of executors (YARN containers) available for the application. Use 2 containers (overall number of containers at the cluster is 64).


After that you can open http://mipt-client.atp-fivt.org:<PORT> in the browser and enter your TOKEN.

The notebook is consuming N+1 YARN containers all the time, so please turn off your notebooks after finishing your work.

If the port for the Jupyter-notebook is unavailable from the outside, you need to forward it when connecting with ssh, in the same way as SparkHistory. Ports 30000 - 30100 should be open. ```ssh USER@mipt-client.atp-fivt.org  -L 30042:mipt-client:30042 -L 30042:mipt-master:30042```
