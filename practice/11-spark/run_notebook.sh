#! /usr/bin/env bash

PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python3 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=<PORT> --NotebookApp.token="<TOKEN>" --no-browser' pyspark2 --master=yarn --num-executors=<N>
