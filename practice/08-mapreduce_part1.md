## Practice: MapReduce-1

Find your username for the cluster in an email from automation@atp-fivt.org and connect to `<username>@mipt-client.atp-fivt.org -L 8088:mipt-master:8088 -L 19888:mipt-master:19888 -L 8888:mipt-node03:8888`;

In case of any issues with access or keys, use username `hdfsuser` and the same password. It's undesirable, however, since you'll have a common job queue.

### The MapReduce programming model

![IMAGE ALT TEXT HERE](images/MapReduce_scheme.png)

#### 1. WordCount
Suppose we need to count the number of occurrences of each word in a given text. Here's how we can solve this using the MR paradigm.

1. **Read**. Read the data from the specified path, split it into batches (usually batch size = HDFS block size) and distribute them between mappers.
2. **Mapper**. Receive lines one by one and yield `(word, 1)` for each word in a line.
3. **Shuffle & sort**. Sort the pairs by key (= word) and send them further so that all pairs with the same key go to the same reducer.
3. **Reducer**. Sum values with the same key. Data received by a reducer is ordered by keys, i.e. it can't receive `k2` while there're still some pairs with `k1`. Yield pairs `(word, number)`.

```bash
OUT_DIR="streaming_wc_result"
NUM_REDUCERS=8

hadoop fs -rm -r -skipTrash $OUT_DIR* # remove the results of previous run (HDFS doesn't overwrite data and will otherwise fail, saying that path is non-empty)

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \  # Hadoop Streaming tools
    -D mapreduce.job.reduces=${NUM_REDUCERS} \  # set the number of reducers
    -files mapper.py,reducer.py \  # add files to distributed cache so that each node would have access to them
    -mapper mapper.py \  # mapper.py should be executable to pass it like this
    -reducer reducer.py \  # otherwise, you must write `python mapper.py`, `bash mapper.py` etc., depending on the language
    -input /data/wiki/en_articles_part \  # input and output data
    -output $OUT_DIR # relative path (= /user/par2018XX/${OUT_DIR})

# check the results
# each reducer writes the output to its own file named `part-XXXXX`
for num in `seq 0 $(($NUM_REDUCERS - 1))`
do
    hdfs dfs -cat ${OUT_DIR}/part-0000$num | head  # print the first 10 lines of each file
done
```
Sources: `/home/velkerr/seminars/pd2020/01-wordcount`.

Let's look at ApplicationMaster: http://localhost:8088/cluster (reconnect if you didn't forward port 8088). Here you can track the job status.
"History" is unavailable even with port forwardign. You should copy the address to another browser tab and manually replace "mipt-master" with "localhost";

To ease things, you can configure a SOCKS proxy (e.g. using FoxyProxy in Firefox) and connect to AppMaster at http://mipt-master.atp-fivt.org:8088/

To distinguish your job from others, it's convenient to name it. Just add `-D mapred.job.name="my_wordcout_example"` to the streaming driver.

**Disable Reduce stage in a job and launch it. What happens?**

#### 2. Debug MapReduce jobs

**Emulate Hadoop with a bash script**
1. Download a part of data from HDFS to a local file system (can be found in /home/velkerr/seminars/pd2020/01-wordcount/in).
2. Run `cat my_data.txt | ./mapper.py | sort | ./reducer.py`.

Source: `/home/velkerr/seminars/pd2020/01-wordcount/test_local.sh`

**Run Hadoop locally**
1. The script looks the same as in distributed case, but add a special config after `yarn`: `--config /opt/cloudera/parcels/CDH/etc/hadoop/conf.empty`
2. Your job will then communicate with the local file system instead of HDFS.

While running locally, there will always be a single reducer, regardles of how many you've specified.

Source: `/home/velkerr/seminars/pd2020/01-wordcount/run_local.sh`

#### 3. Problem
In the example output, you could see some garbage that wasn't even words. Now you should
- get rid of punctuation marks while counting words
- ignore the letter case.

#### 4. Combiner
It may so happen that the immediate output of a mapper can already be aggregated somehow. If we do that **before** the reduce stage, this will save some resources. To that end, we can use a **combiner**. Its formal model coincides with reducer's: `(k, v1), (k, v2) -> (k, [v1, v2])`; but the implementation has two distinctions:
* a combiner is called on the output of a single mapper,
* you've no guarantee how many times (if any at all) a combiner will be called

Do we need a combiner in our example? How would it look?

#### 5. Multiple jobs
In real life, Hadoop programs comprise many jobs that are executed in some order and constitute a directed acyclic computation graph. Let's sort the words by number of occurrences.

Source: `/home/velkerr/seminars/pd2020/03-chaining/run.sh`

#### 6. Problem
Filter out stop words while counting occurrences. The list of stop words can be found in `/datasets/stop_words_en.txt`. To work with a file, you must add it to DistributedCache.

#### 7. Hadoop Counters
 - another way to debug
 - allows to avoid using reducers in some cases

Source: `/home/velkerr/seminars/pd2020/06-counters`

Declare a counter in python3: `print("reporter:counter:Wiki stats,Stop words,{}".format(1), file=sys.stderr)`
* `reporter:counter:` - counter declaration
* `Wiki stats` - a group of counters
* `Stop words` - the name of the counter

#### 8. Calculate Pi
Estimate the value of Pi using a Monte Carlo method.

Source: `/home/velkerr/seminars/pd2020/07_pi_stub`
