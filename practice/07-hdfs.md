## Practice: HDFS ##

### Cluster structure and server roles ###
Cluster:

![IMAGE ALT TEXT HERE](images/MIPT%20Hadoop%20cluster%20architecture.jpg)

Client server:

    mipt-client.atp-fivt.org

Connection:

	$ ssh USER@mipt-client.atp-fivt.org

Our cluster can only be accessed from the client server, which you can connect to via ssh. In normal circumstances you shouldn't access cluster nodes directly, use client utilities (hdfs, yarn, hadoop) instead.

For educational purposes, however, we're going to do that, in order to look into how data storage is organised. There's a special user for that:

    login: hdfsuser
    password: hdfsuser

Any datanode (for instance, mipt-node08) can be accessed using this command:

	$ sudo -u hdfsuser ssh hdfsuser@mipt-node08.atp-fivt.org


### Primary commands ###

(all commands below should be executed on the client server)

* Directory contents:

		$ hdfs dfs -ls /
  		$ hdfs dfs -ls .
  		$ hdfs dfs -ls

    the last two are equivalent and refer to the home directory; show where it's located in hdfs `(/user/...)`

* Create a directory, load a text file into it:

		$ hdfs dfs -mkdir <dir>
 		$ hdfs dfs -put <local_file> <hdfs_file>
		$ hdfs dfs -cat <file>

    In the last one, `-cat` could be replaced with `-text`. `-text` recognizes the encoding and decodes the file; `-cat` can't do that. We'll need this later for working with OutputFormat's in MapReduce.
	Create a text file:

		$ echo "read it" > README
		$ echo "read it again" >> README
		$ cat README
    Load it into HDFS, print it from there using `-cat`.

* Rename & copy:

		$ hdfs dfs -mv <file1> <file2>
 		$ hdfs dfs -cp <file1> <file2>
    NB: copying is done through the client. To avoid that, or even perform a copy operation between different clusters, use `hadoop distcp`.

* Explore directory /data

		$ hdfs dfs -ls /data
    Print a part of a file using `more` (`less`) and `head`. For instance, the first two lines from the file with Wikipedia articles:

		$ hdfs dfs -cat /data/wiki/en_articles/articles | head -2
		$ hdfs dfs -tail /data/wiki/en_articles/articles

        Can you explain why `-tail` is implemented in HDFS, but `-head` is not?

* Explain the output of `ls`:

		$ hdfs dfs -ls /data/wiki/en_articles
		Found 1 items
		-rw-r--r--   3 hdfs supergroup 12328051927 2017-07-03 23:21 /data/wiki/en_articles/articles
    prints permissions, number of replicas, user, group, size and modification timestamp.
    Confirm that it's what `hdfs dfs -help` promises us.

* Estimate file space usage:

		$ hdfs dfs -du -h /data/wiki
        11.5 G  34.4 G   /data/wiki/en_articles
	prints the size and the total occupied space. The last figure takes replication factor into account (3 by default, 2 in our case).

* Change replication factor:

		$ hdfs dfs -setrep <path>
	Check the effect of this command with `ls`.
	(add `-w` flag if you want to wait for the command to finish instead of executing asynchronously)

* Remove a file:

		$ hdfs dfs -rm <file>
  or a directory:

		$ hdfs dfs -rm -r <file>

  The specified file is moved to `~/.Trash`, and the absolute path is emulated (`.../.Trash/user/your_user/your_dir/your_file`). To bypass trash and remove the file completely, add `--skipTrash`.

For other commands, see `-help`.

### Namenode Web UI ###

Go to [http://mipt-master.atp-fivt.org:50070](http://mipt-master.atp-fivt.org:50070)

If you can't access it, forward port 50070:

	$ ssh -L 50070:mipt-master.atp-fivt.org:50070 MY_USER@mipt-client.atp-fivt.org  # Enter your username (`par***`) in place of MY_USER

While ssh session is active, the interface will be located here: [http://localhost:50070](http://localhost:50070)

Show statistics:

- Heap memory used/total - total amount of memory on NN and current memory usage. The latter shouldn't be too large.
- Capacity - total DFS space
- DFS Used - used DFS space
- Live nodes - number of active datanodes and their usage
- Utilities -> Browse the Filesystem

### Data storage architecture ###
#### HDFS fsck ####

Get blocks into which a file is split, their sizes, id's and locations:

	$ hdfs fsck /data/wiki/en_articles_part -files -blocks -locations

Get info for a single block:

	$ hdfs fsck -blockId blk_1073971176

#### WebHDFS REST API ####

Get the address of a datanode where the file data is located:

	$ curl -i "http://mipt-master.atp-fivt.org:50070/webhdfs/v1/data/wiki/en_articles_part/articles-part?op=OPEN"

Copy `Location` from the output of this command and add `&length=100` to get the first 100 symbols in the file.

See more at http://hadoop.apache.org/docs/r1.2.1/webhdfs.html

#### Java API

Example: print top 5 lines of a dataset that is stored in HDFS (directory path is passed through `args`).

```java
public class OutputTopAndDelete {

    public static void main(String[] args) throws IOException {
        Path outPath = new Path(args[0]);
        FileSystem fs = FileSystem.get(new Configuration()); file system attributes
        int top = 5;
        if(!fs.exists(outPath)){
            System.err.println("Incorrect path!");
            return;
        }
        outTop(fs, outPath, top);
        fs.deleteOnExit(outPath); // delete data before exiting (we'll elaborate on that later on MapReduce class)
    }

    /**
     * Useful when your data is sorted, for instance.
     */
    private static void outTop(FileSystem fs, Path path, int top) throws IOException {
        int linesCount = 0;
        for (FileStatus status: fs.listStatus(path)){ //FileStatus describes a file (or a directory) in HDFS (path, creation timestamp, permissions...)
            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status.getPath())));
            String line = null;
            // read lines one by one until we get `top` lines
            while ((line = br.readLine()) != null && linesCount < top){
                System.out.println(line);
                linesCount++;
            }
        }
    }
}

```

* Maven is used for building the project.
* Run: `hadoop jar <JAR> [args]` (see `run.sh`).

See full source code [here](https://gitlab.com/pd2020-supplementary/foreigners/-/tree/master/practice/code/hdfs_example).

#### Python API
Let's configure the environment first.

1. Create file ~/.hdfscli.cfg
2. Save the FS configuration there:
```
[global]
default.alias = default

[default.alias]
url = http://mipt-master.atp-fivt.org:50070
user = <my_user> # for example, velkerr
```

3. Now we can use the API.
```python
>>> from hdfs import Config
>>> client = Config().get_client()
>>> client.list('/data')
['MobodMovieLens', 'access_logs', 'course4' ... ]
```

[More examples](https://hdfscli.readthedocs.io/en/latest/quickstart.html#reading-and-writing-files)
* reading,
* writing,
* downloading to and uploading from a local FS

Get file status (analogous to FileStatus in Java API).
```python
>>> client.status('/data/wiki')
{'accessTime': 0, 'length': 0, ...}
```

More python packages: hadoopy pydoop dumbo mrjob .

### Estimate cluster parameters ###

**Task 1:**
For an HDFS cluster with capacity = 2PB, block size = 64MB and replication factor = 3 estimate the minimum amount of RAM on the namenode. We'll assume that metadata for each block (regardless of the number of replicas) occupies 600B.

<details>
  <summary>Solution</summary>
  
2PB / 64MB = number of blocks.

Taking replicdation factor into account, there will be 2PB / 64MB / 3 unique blocks.

Memory needed:
(2PB / 64MB / 3) * 600B = 6.25GB
  
</details>


**Task 2:**
HDDs with following parameters are used in the cluster: seek time = 12ms, linear reading speed = 210 MB/s. What is the minimum block size for which locating a block would take no more than 0.5% of the time required to read this block?

<details>
  <summary>Solution</summary>
  
If block size = X MB, then 0.5% of read time should be less or equal to seek time:

X / (210 MB/S) * 0.005 $`\geq`$ 0.012s

X $`\geq`$ 0.012 * 210 / 0.005 = 504 MB
  
</details>


### hdfs-shell

The default HDFS shell
* is stateless
* starts a new Java Virtual Machine for each command
* doesn't support autocompletion
* doesn't allow you to change user (`su ...`).

In 2017, Avast developed a wrapper: https://github.com/avast/hdfs-shell. It's written in Java, but still looks like bash to the user.

Installation:
* download binary (wget),
* unzip, add ./libs to CLASSPATH
* `cd` to hdfs-shell-1.0.7/bin
* run hdfs-shell.sh.

On our cluster, the wrapper shell is already provided, just run `hdfs-shell`.

Pros.
* Shortened commands (e.g. ls, instead of hdfs dfs -ls; both will work though). See `help` for more.
* Autocompletion,
* Advanced commands that are present in bash but absent in standard hdfs shell (e.g. pwd, groups).
* Create your own commands and save them.

Cons.
* Commands cannot be piped (e.g. `ls -l | grep`).
* `su` allows changing user without authorization, which is unsafe.

### Apache Hue (hadoop user experience)
1. Forward port `8888:mipt-node03:8888`
2. Username `hue_user`, password `hue_userpd`

### Testing system for your homeworks ###

* You've been assigned a repository named `<your name>-demo`, which has a branch `demotask1`. Your repos are going to look like this.
* Download the contents of http://gitlab.atp-fivt.org/root/demos (that's WordCount for MapReduce) and commit them to branch `demotask1` in your repo.
* Examine the pipeline and testing system logs.
* Now let's insert a but into our code. On the last line of `mapper.py`, replace `1` with `2`.
* Commit, push, examine the output again and behold some failed tests.

[More](https://docs.google.com/presentation/d/1eDxnTeBWSB1OrA3BwEUa2vJAJm3_OJMuoRxyA13RzTY/edit#slide=id.p)
