### Hive. User-defined functions

* Regular UDF: takes 1 row, produces 1 row
* User-defined table function (UDTF): takes 1 row, produces $`n`$ rows
* User-defined aggregate function (UDAF): takes $`n`$ rows, produces 1 row
* Window function: takes $`m`$ rows (a "window"), produces $`n`$ rows. Built-in aggregate functions and UDAFs can also be used as window functions.

#### I. (Regular) User-defined functions

1. To declare a UDF, you must create a java class that extends `org.apache.hadoop.hive.ql.exec.UDF`.
2. Implement one or more `evaluate()` methods containing the logic.
3. For building, add this jar:
```
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-exec.jar
```
4. To use the UDF in a query:

  a) add the resulting jar file to the distributed cache (relative paths allowed):
```
ADD JAR <path_to_jar>
```

No additional jars are required, since the jar with your UDF already contains everything necessary.

  b) create a function based on your java class:
```
CREATE TEMPORARY FUNCTION <your_udf> AS 'com.your.OwnUDF';
```
> **Example**. Implement an identity function

You can use this example to examine the UDF syntax and reuse it in further tasks. Source:
```
/home/velkerr/seminars/pd2020/13-hive_adv/1-example_udf[example.sql, Identity/]
```

> **Example.** Implement a UDF that takes an IP address and produces the sum of its octets. You can use either "Subnets" or "SerDeExample" table as input data, since they both contain a field with IPs.

With UDF:
* too much code,
* **Java** only :(

Without UDF:
* even more code (but in SQL). Example: `/home/velkerr/seminars/pd2020/13-hive_adv/2-sum_udf/query_without_udf.sql`
* It's not always possible to fit the logic in one query => subqueries => several jobs => greater execution time

#### II. User-defined table functions (UDTF)
These differ from regular UDFs in that they can produce more than one row per call. The number of columns in output is also not constrained, i.e. we can get a whole table from a single row; hence the name.

1. To implement a UDTF, extend `org.apache.hadoop.hive.ql.udf.generic.GenericUDTF`.
2. The logic is split into 3 methods:

   а) `initialize()`:
     - parse the input (check number of arguments and their types), save the data to ObjectInspectors
     - create the output table structure (field names and data types)

   б) `process()`: transformation itself,

   в) `close()`: analogous to `cleanup()` in  MapReduce. Notify that there are no more rows to process, clean up code or produce additional output.

3. When building, add two additional external jars instead of one:
```
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-exec.jar
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar
```

> **Example**. Implement a UDTF that takes an IP address and produces 2 copies of it. Print the separator "-----" between outputs for different IPs.

|Input|Output|
|:----:|:---:|
|60.143.233.0|60.143.233.0|
||60.143.233.0|
||-----|
|14.226.82.0|14.226.82.0|
||14.226.82.0|
||-----|

The UDTF and a query using it can be found here:
```
/home/velkerr/seminars/pd2020/13-hive_adv/3-example_udtf[example.sql, CopyIp/]
```
> **Problem**. Implement a UDTF that takes an IP address and produces the list of its octets. Separate outputs for different IPs with `Integer.MAX_VALUE`.

|Input|Output|
|:----:|:---:|
|60.143.233.0|60|
||143|
||233|
||0|
||2147483647|
|14.226.82.11|14|
||226|
||82|
||11|
||2147483647|

#### III. User-defined aggregate functions (UDAF)

These are functions like built-in `SUM()`, `COUNT()`, `AVG()`.

**Literature.** [Programming hive](https://www.gocit.vn/files/Oreilly.Programming.Hive-www.gocit.vn.pdf), chapter 13 "Functions" (p. 163).

### Optimizing Joins

Database `velkerr_test` contains two tables:

**Logs**
* ip (string)
* date (int)
* request (string)
* pagesize (smallint)
* statuscode (smallint)

**IpRegions**
* ip (string)
* region (string)

Let's join then with disabled MapJoin:

```sql
USE velkerr_test;
SET hive.auto.convert.join=false;

SELECT * FROM logs LEFT JOIN ipregions ON logs.ip = ipregions.ip
LIMIT 10;
```
* wall time on an idle cluster is 15+ seconds.
* Hue Job browser and YARN JobHistory show that a reducer has been generated.

Let's examine the size of IpRegions. `SELECT count(1) FROM ipregions;` prints 10, so it should fit into RAM. MapJoin can therefore be used (recall what's a Map-side join in Hadoop).

Two ways exist to tell Hive to use MapJoin.

Use a hint, which looks like a C-style comment (isn't guaranteed to work):
```sql
USE velkerr_test;

SELECT /*+ MAPJOIN(ipregions) */ * FROM logs LEFT JOIN ipregions ON logs.ip = ipregions.ip
LIMIT 10;
```

Let Hive do it automatically:
```sql
USE velkerr_test;
SET hive.auto.convert.join=true;

SELECT * FROM logs LEFT JOIN ipregions ON logs.ip = ipregions.ip
LIMIT 10;
```
Run and check that the reducer has vanished and the execution has sped up (~5s on an idle cluster).

MapJoin can't be used when the small table is on the left-hand side of `LEFT JOIN` or on the right-hand side of `RIGHT JOIN`.

[More about MapJoin](https://cwiki.apache.org/confluence/display/Hive/LanguageManual+JoinOptimization#LanguageManualJoinOptimization-PriorSupportforMAPJOIN).
