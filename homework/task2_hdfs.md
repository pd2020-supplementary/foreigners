## Task 2. HDFS

In this homework you're going to get more intimately familiar with [HDFS](http://hadoop.apache.org/docs/r1.2.1/hdfs_design.html), a distributed file system. Your objective is to write several scripts that will perform http requests to various HDFS daemons or call standard shell utilities to get some information.

You can use any scripting language. The simplest option is probably bash with utilities like curl, grep, wc etc.

<h3> Deadlines </h3>

| Soft deadline | Hard deadline (-50%) |
|  ------  |------|
| March 22, 23:59 | March 22, 23:59 |

You can fix issues within a month after the code review.


### How to submit your code

For each hometask, you've got a repo with branch `master` and a branch for each subtask. To hand in completed work, you must do the following in each branch:
1. Put a file named `.gitlab-ci.yml` with the text below into the root directory:
```yml
job1:
  except:
    - master
  script:
    - date
    - if [ `grep -v '#' ${CI_COMMIT_REF_NAME}/run.sh | wc -l` -gt 0 ]; then (cd ~/code; ./gitlab_ci_runner.py); else exit 1; fi
```
(you can just download it [here](http://gitlab.atp-fivt.org/root/demos/blob/ci_files/.gitlab-ci.yml))

2. Create a directory with the same name as the branch. E.g. directory `hdfstask1` in branch `hdfstask1`.

3. This directory should contain a file named `run.sh`, which will serve as the entry point into your program and will be executed by the testing system. `run.sh` can either contain the entire solution or call other files.

> Even if you develop on python, there should be a `run.sh` containing, for instance, just a call to the necessary python script.
>
> 	#!/usr/bin/env bash
> 	
> 	python my_python_script.py $*
>
> Here $* forwards parameters with which `run.sh` was called to `my_python_script.py`.

### Subtasks

#### 01 (0.1 points)
Given a file name, print the server name or ip address from which the first data block of this file will be read (in case of multiple replicas, any of them will be accepted). Example:

	$ ./run.sh /data/access_logs/big_log/access.log.2015-12-10
	mipt-node01.atp-fivt.org

#### 02 (0.1 points)
Given a file name, print the first 10 bytes of this file (you can't use `hadoop fs` or `hdfs dfs`).

	$ ./run.sh /data/access_logs/big_log/access.log.2015-12-10
	41.190.60.

#### 03 (0.1 points)
Given a full path to some file in HDFS, print this file's size in blocks (see `hdfs fsck -h`).

	$ ./run.sh /data/access_logs/big_log/access.log.2015-12-10
	8

#### 04 (0.3 points)
Given a block id, print the name of the server (any of them if several) where this block is stored and physical path to this block in the local file system (see [HDFS practice class](https://gitlab.com/pd2020-supplementary/foreigners/-/blob/master/practice/07-hdfs.md) on how to access cluster nodes).

	$ ./run.sh blk_1075127191
	bds03.vdi.mipt.ru:/dfs/dn/current/BP-76251478-10.55.163.141-1427134131440/current/finalized/subdir21/subdir35/blk_1075127191

#### 05 (0.4 points)
Large files are split into blocks of a certain size. You should find out the amount of additional memory used by HDFS to store some data with replication factor = 1 (i.e. without any backups). For experimenting with files of different size, we suggest using `dd` (see [usage example](http://unix.stackexchange.com/questions/101332/generate-file-of-a-certain-size)), combined with your solution of subtask 4. Your script is given the file size in bytes and should print a single number equal to this size subtracted from number of bytes required to physically store this file (not counting files with .meta extension),  

	$ ./run.sh <int>
	123
